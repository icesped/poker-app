import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Card } from '../models/card'
import '../services/rxjs/index';

@Injectable()
export class DealerService {
	private API_URI = "https://services.comparaonline.com/dealer/"
	public isTokenExists: boolean = false;
	public tryGetToken = false;

    constructor(private http: Http) { }

	/**
	 * Obtiene el token
	 */
    getTokenFromServer(): Observable<String> {
        let URL = this.API_URI + 'deck';
		return this.http.post(URL, '', { headers: this.getHeaders() })
		.delay(2500)
		.map((res: Response) => {
			this.isTokenExists = true;
			window.localStorage.setItem('token', res.text())
		}).catch(err => this.handleError(err).do(res => this.isTokenExists = false))
	}

	/**
	 * Obtiene una mano de cartas
	 */
	getCardsFromServer(amount: Number): Observable<Card[]> {
		let token = localStorage.getItem('token');
		let URL = this.API_URI + 'deck/' + token + '/deal/' + amount

		return this.http.get(URL, { headers: this.getHeaders() })
		.map((res: Response) => res.json())
		.catch(err => this.handleError(err))
	}


	/**
   	* Obtiene las cabeceras 
   	*/
    private getHeaders() {
        let headers = new Headers();
        headers.append('Accept', 'application/json');

        return headers;
	}
	
    /**
      * Maneja los errores.
      */
    private handleError(error: any) {
		let errorBody = error.json();
		let errorMessage = ''
		
		switch(errorBody.statusCode) {
			case 500:
				errorMessage = 'No hemos podido establecer conexión con el servidor, vuelve a intentarlo.'
				break;
			case 405:
				errorMessage = 'No quedan cartas en la baraja, favor vuelve a iniciar el juego.'
				this.isTokenExists = false;
				this.tryGetToken = false;
				break;
			case 404:
				errorMessage = 'La sessión a expirado, favor vuelve a iniciar el juego.'
				this.isTokenExists = false;
				this.tryGetToken = false;
				break;
			default:
				errorMessage = 'Se ha producido un error. Favor vuelve a intentarlo.'
				break;
		}

        return Observable.throw(errorMessage);
    }
}