export interface Card
{
    number: string;
    suit: string;
}
