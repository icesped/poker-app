import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

let modules = [
    HttpModule,
    BrowserModule,
    FormsModule,
]

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

let components = [
    AppComponent,
    DashboardComponent
];

import { DealerService } from './services/dealer.service';

let services = [
    DealerService
]

@NgModule({
  declarations: [ ...components ],
  imports: [ ...modules ],
  providers: [ ...services ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
