import { Component } from '@angular/core';
import { DealerService } from '../../services/dealer.service'
import { Card } from '../../models/card'
import { PokerHandResult } from '../../models/pokerHand'
declare var swal: any;

@Component({
    moduleId: module.id,
    selector: 'dashboard',
    templateUrl: 'dashboard.html',
    styleUrls: ['dashboard.css']
})

export class DashboardComponent {
    public CARDS_AMOUNT: number = 5

    public tryGetToken: boolean = false;
    public existWinner: boolean = false;
    public playerOneCards: Card[] = [];
    public playerTwoCards: Card[] = [];
    public winningPlay = ''
    public winnerName = ''
    public highCards = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];

    constructor( private dealerService: DealerService ) {}

    /**
     * Obtiene el token e inicia el juego.
     */
    tryBeginPokerGame() {
        this.dealerService.tryGetToken = true;

        this.dealerService.getTokenFromServer().subscribe(
            res => {
                console.log("Comienza el Juego");
            },
            err => {
                swal(
                    'Ha ocurrido un error!',
                    err,
                    'error'
                )
                this.dealerService.tryGetToken = false;                
            }
        )
    }

    /**
     * Obtiene una mano de cartas dependiendo del jugador que la solicita.
     * @param player 
     */
    getPokerCards(player) {
        // Oculta la seccion del resultado de la jugada.
        this.existWinner = false;

        this.dealerService.getCardsFromServer(this.CARDS_AMOUNT).subscribe(
            cards => {
                if (player === 'one'){
                    this.playerOneCards = cards;
                }
                else
                    this.playerTwoCards = cards;
            },
            err => {
                swal(
                    'Ha ocurrido un error!',
                    err,
                    'error'
                )
            }
        )
    }

    /**
     * Comprueba quien ha ganado el juego.
     */
    getWinner() {

        // Evalua cada mano y obtiene el puntaje.
        let playerOneScore = this.getPokerHandScore(this.playerOneCards)
        let playerTwoScore = this.getPokerHandScore(this.playerTwoCards)

        // Verifica quien es el ganador de la partida
        if (playerOneScore.value > playerTwoScore.value) {
            this.winningPlay = playerOneScore.name;
            this.winnerName = 'Jugador 1';
        } else if (playerTwoScore.value > playerOneScore.value) {
            this.winningPlay = playerTwoScore.name;
            this.winnerName = 'Jugador 2';
        } else {
            this.winningPlay = 'Empate';
            this.winnerName = 'Empate';
        }
        this.existWinner = true;
    }

    /**
     * Obtiene un objeto PokerHand con los valores de la mano.
     * @param cardsArrays 
     */
    getPokerHandScore (cardsArrays: Card[]): PokerHandResult
    {
        // evalua y obtiene la mano 
        let cards = this.getBestPokerHand(cardsArrays);
        
        // Obtiene la mano  resultante
        let bestHand = this.getHandResult(cards);

        // obtiene el mejor resultado evaluando las reglas de poker.
        for (let card of [cards]) {
            let result = this.getEvaluatedHand(card);

            if (result.value > bestHand.value)
                bestHand = result;
        }
        
        return bestHand;
    }

    /**
     * Evalua las High Cards
     * @param cardsArrays 
     */
    getBestPokerHand (cardsArrays: Card[]): Card[]
    {
        // concatena
        let cards: Card[] = [].concat.apply([], cardsArrays);
        
        // valida la clasificación y la pinta
        cards = cards.filter((card) => {
            return !!(this.highCards.indexOf(card.number) > -1 && card.suit);
        });

        return cards;
    }

    /**
     * Obtiene las cartas clasificadas y ordenadas.
     * @param cards 
     */
    getCardsSorted (cards: Card[]): Card[][]
    {
        let result: Card[][] = [];

        // divide las cartas por su clasificación
        for (let card of cards) {
            let index = this.highCards.indexOf(card.number);
            result[index] = result[index] || [];
            result[index].push(card);
        }

        // limpia los valores undefined
        result = result.filter((rank) => !!rank);
        
        // ordena de mayor a menor
        result.reverse();

        // Ordena para que queden pares y conjuntos en primer lugar
        result.sort((a, b) => {
            return a.length > b.length ? -1 : a.length < b.length ? 1 : 0;
        });

        return result;
    }

    /**
     * Verifica que la mano tenga valores consecutivos.
     * @param cards 
     */
    isStraight (cards: Card[][]): boolean
    {
        // Valida que existan 5 cartas
        if (!cards[4])
            return false;

        // Ordena la A (As) y lo coloca al final de array
        if (cards[0][0].number == 'A' && cards[1][0].number == '5' && cards[4][0].number == '2') {
            cards.push(cards.shift());
            return true;
        }

        // si no, valida la existencia en el array de cartas highCards
        let i1 = this.highCards.indexOf(cards[0][0].number);
        let i4 = this.highCards.indexOf(cards[4][0].number);
        return (i1 - i4) == 4;
    }

    /**
     * Verifica que todas las cartas sean de la misma pinta.
     * @param cards 
     */
    isFlush (cards: Card[]): boolean
    {
        let suit = cards[0].suit;

        for (let card of cards) {
            if (card.suit != suit)
                return false;
        }

        return true;
    }

    /**
     * Obtiene el puntaje de la mano evaluada.
     * @param cards 
     * @param primary 
     */
    getHandScore (cards: Card[][], primary: number): number
    {
        let aux = '';

        for (let card of cards) {

            // Obtiene el indice y el valor
            let cardIndex = this.highCards.indexOf(card[0].number);
            let value = (cardIndex < 10 ? '0' : '') + cardIndex;

            for (let i = 0; i < card.length; i++) {
                // suma el valor de cada carta
                aux += value;
            }
        }

        // Incrementa el valor para diferenciar al ganador.
        return (primary * 10000000000) + parseInt(aux);
    }

    /**
     * Devuelve un objeto con el resultado de la mano.
     * @param cards 
     * @param name 
     * @param value 
     */
    getHandResult(cards: Card[], name?: string, value?: number): PokerHandResult
    {
        return {
            cards: cards,
            name: name || 'nothing',
            value: value || 0
        };
    }

    /**
     * Evalua que condicion del juego se cumple y devuelve el resultado.
     * @param cards 
     */
    getEvaluatedHand (cards: Card[]): PokerHandResult
    {
        let sortedCards: Card[][] = this.getCardsSorted(cards);
        let isFlush = this.isFlush(cards);
        let isStraight = this.isStraight(sortedCards);
        
        if (isStraight && isFlush && sortedCards[0][0].number == 'A')
            return this.getHandResult(cards, 'Royal Flush', this.getHandScore(sortedCards, 9));

        else if (isStraight && isFlush)
            return this.getHandResult(cards, 'Straight Flush', this.getHandScore(sortedCards, 8));

        else if (sortedCards[0].length == 4)
            return this.getHandResult(cards, 'Four of a Kind', this.getHandScore(sortedCards, 7));

        else if (sortedCards[0].length == 3 && sortedCards[1].length == 2)
            return this.getHandResult(cards, 'Full House', this.getHandScore(sortedCards, 6));

        else if (isFlush)
            return this.getHandResult(cards, 'Flush', this.getHandScore(sortedCards, 5));

        else if (isStraight)
            return this.getHandResult(cards, 'Straight', this.getHandScore(sortedCards, 4));

        else if (sortedCards[0].length == 3)
            return this.getHandResult(cards, 'Three of a Kind', this.getHandScore(sortedCards, 3));

        else if (sortedCards[0].length == 2 && sortedCards[1].length == 2)
            return this.getHandResult(cards, 'Two Pair', this.getHandScore(sortedCards, 2));

        else if (sortedCards[0].length == 2)
            return this.getHandResult(cards, 'One Pair', this.getHandScore(sortedCards, 1));

        else
            return this.getHandResult(cards, 'High Card', this.getHandScore(sortedCards, 0));

    }
}