import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  template: `
  <div style="background-image: url(./assets/poker-background.jpg)" class="background-image">
    <dashboard></dashboard>
  </div>
  `,
  styles: [`
  .background-image {
    background-size: cover;
    width: 100%;
    height: 100vh;
  }`
]})

export class AppComponent {
    public constructor(private titleService: Title) {}

    ngOnInit() {
      this.titleService.setTitle('ComparaOnline - PokerApp');
  }
}
