# PokerApp

Este proyecto fue generado con el CLI de Angular [Angular CLI](https://github.com/angular/angular-cli) versión 1.0.0.

## Para Levantar el Servidor

Una vez clonado el proyecto, se debe ejecutar el comando `npm install` para instalar todas las dependencias.
Luego, solo se debe ejecutar el comando `ng serve` para que Angular levante el servidor de desarrollo en `http://localhost:4200/` y comience el juego.

## Sobre el Proyecto

Para este proyecto se utilizó `Angular` en su versión 4. Fue escrito en `TypeScript` y se utilizó `ECMAScript 2016`. Adicionalmente se utilizó la librería RxJs, la que permite utilizar programación Reactiva mediante Observables. 

Se controlaron errores del servidor y se muestran alertas al usuario mediante `sweetAlert2`.

## Sobre el Juego

En la pantalla inicial de bienvenida, se debe hacer click en el botón para comenzar el juego, esto gatilla la llamada al servicio que se comunica con el servidor de ComparaOnline para obtener el `token` que permite solicitar cartas posteriormente. Este token se almacena en `localStorage` del navegador.

Luego, en la pantalla de juego, cada jugador debe solicitar una mano de cartas haciendo click en su respectivo botón. Una vez recibidas las cartas (dependiendo de la respuesta del servidor), aparecerán en pantalla el detalle de estas (Valor y Pinta).

Finalmente, estará disponible el botón para evaluar ambas manos de cartas y se mostrara al ganador y la jugada ganadora.

En caso de que las `cartas se acaben o expire el token` del juego, el usuario será notificado y volverá a la pantalla inicial para volver a iniciar el juego.
